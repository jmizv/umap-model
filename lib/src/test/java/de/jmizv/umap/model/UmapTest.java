package de.jmizv.umap.model;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class UmapTest {

    @Test
    void should_generate_map() {
        var map = Umap.of("Map test", PointGeometry.of(new double[]{51, 12}), List.of());
        Assertions.assertThat(map.properties().name()).isEqualTo("Map test");
    }
}
