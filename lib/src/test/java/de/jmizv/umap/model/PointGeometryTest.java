package de.jmizv.umap.model;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class PointGeometryTest {
    @Test
    void should_throw_on_illegal_coordinates() {
        assertThatThrownBy(() -> PointGeometry.of(new double[]{}))
                .isInstanceOf(IllegalStateException.class);
        assertThatThrownBy(() -> PointGeometry.of(new double[]{1.5}))
                .isInstanceOf(IllegalStateException.class);
        assertThatThrownBy(() -> PointGeometry.of(new double[]{1.5, 8181, 123456789.12}))
                .isInstanceOf(IllegalStateException.class);
    }

}