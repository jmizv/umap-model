package de.jmizv.umap.model;

import org.immutables.value.Value;

import java.util.Arrays;

@Value.Immutable
public abstract class PointGeometry extends Geometry {
    private final String type = "Point";

    public abstract double[] coordinates();

    @Value.Check
    protected void check() {
        if (coordinates().length != 2) {
            throw new IllegalStateException("Expected to have a double array with two values but got: " + Arrays.toString(coordinates()));
        }
    }

    public static PointGeometry of(double[] coordinates) {
        return ImmutablePointGeometry.builder()
                .coordinates(coordinates)
                .build();
    }
}
