package de.jmizv.umap.model;

public enum IconClass {

  DEFAULT(""),
  CIRCLE("Circle"),
  DROP("Drop"),
  BALL("Ball");

  private final String value;

  IconClass(String value) {
    this.value = value;
  }

  public String toString() {
    return value;
  }
}
