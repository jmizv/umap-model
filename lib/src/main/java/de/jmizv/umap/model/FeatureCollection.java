package de.jmizv.umap.model;

import org.immutables.value.Value;

import java.util.Collection;
import java.util.List;

@Value.Immutable
public abstract class FeatureCollection {
    private final String type = "FeatureCollection";
    public abstract List<Feature> features();

    public static FeatureCollection ofFeatures(Collection<Feature> features) {
        return ImmutableFeatureCollection.builder()
                .features(features)
                .build();
    }
}
