package de.jmizv.umap.model;

import org.immutables.value.Value;

@Value.Immutable
public abstract class SlideshowProperties {
    public abstract boolean active();

    public abstract int delay();

    public abstract boolean easing();

    public abstract boolean autoplay();

    public static SlideshowProperties ofDeactivated() {
        return ImmutableSlideshowProperties.builder()
                .active(false)
                .delay(1000)
                .easing(false)
                .autoplay(false)
                .build();
    }
}
