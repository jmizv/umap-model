package de.jmizv.umap.model;


import org.immutables.value.Value;

@Value.Immutable
public abstract class Feature {
    private final String type = "Feature";
    public abstract FeatureProperties properties();
    public abstract Geometry geometry();

    public static Feature of(FeatureProperties properties, Geometry geometry) {
        return ImmutableFeature.builder()
                .properties(properties)
                .geometry(geometry)
                .build();
    }
}
