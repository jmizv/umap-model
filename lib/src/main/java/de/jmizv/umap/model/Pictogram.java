package de.jmizv.umap.model;

/**
 * The pictogram images are © OSMIC.
 */
public enum Pictogram {

  // accommodation
  HOSTEL("hostel.svg"),
  HOTEL("hotel.svg"),
  MOTEL("motel.svg"),

  // administration
  COURTHOUSE("courthouse.svg"),
  EMBASSY("embassy.svg"),
  GOVERNMENT("government.svg"),
  PRISON("prison.svg"),
  TOWN_HALL("town-hall.svg"),

  // amenity
  CEMETERY("cemetery.svg"),
  CINEMA("cinema.svg"),
  CLOCK("clock.svg"),
  ENTRANCE("entrance.svg"),
  EXIT("exit.svg"),
  LIBRARY("library.svg"),
  NIGHTCLUB("nightclub.svg"),
  PLAYGROUND("playground.svg"),
  POST_OFFICE("post-office.svg"),
  RECYCLING("recycling.svg"),
  TELEPHONE("telephone.svg"),
  TOILETS("toilets.svg"),

  // barrier
  BOLLARD("bollard.svg"),
  CATTLE_GRID("cattle-grid.svg"),
  GATE("gate.svg"),
  LIFT_GATE("lift-gate.svg"),
  STEPS("steps.svg"),

  // eat-drink
  BAR("bar.svg"),
  BIERGARTEN("biergarten.svg"),
  CAFE("cafe.svg"),
  FAST_FOOD("fast-food.svg"),
  PUB("pub.svg"),
  RESTAURANT("restaurant.svg"),

  // emergency 
  EMERGENCY_PHONE("emergency-phone.svg"),
  FIRE_STATION("fire-station.svg"),
  POLICE("police.svg"),

  // energy
  POWER_WIND("power-wind.svg"),

  // health
  DENTIST("dentist.svg"),
  DOCTOR("doctor.svg"),
  HOSPITAL("hospital.svg"),
  PHARMACY("pharmacy.svg"),
  VETERINARY("veterinary.svg"),

  // money
  ATM("atm.svg"),
  BANK("bank.svg"),

  // nature
  PEAK("peak.svg"),
  SADDLE("saddle.svg"),
  SPRING("spring.svg"),
  TREE_CONIFEROUS("tree-coniferous.svg"),
  TREE_DECIDUOUS("tree-deciduous.svg"),
  TREE_UNSPECIFIED("tree-unspecified.svg"),
  WATERFALL("waterfall.svg"),

  // outdoor 
  ALPINE_HUT("alpine-hut.svg"),
  BASIC_HUT("basic-hut.svg"),
  BENCH("bench.svg"),
  CAMPING("camping.svg"),
  CARAVAN("caravan.svg"),
  DRINKING_WATER("drinking-water.svg"),
  FOUNTAIN("fountain.svg"),
  GUIDEPOST("guidepost.svg"),
  HUNTING_STAND("hunting-stand.svg"),
  LIGHTHOUSE("lighthouse.svg"),
  MAST("mast.svg"),
  MAST_COMMUNICATION("mast-communications.svg"),
  SHELTER("shelter.svg"),
  TABLE("table.svg"),
  WASTE_BASKET("waste-basket.svg"),
  WATER_TOWER("water-tower.svg"),
  WILDERNESS_HUT("wilderness-hut.svg"),

  // religious
  BUDDHIST("buddhist.svg"),
  CHRISTIAN("christian.svg"),
  CHRISTIAN_ORTHODOX("christian-orthodox.svg"),
  HINDUIST("hinduist.svg"),
  JEWISH("jewish.svg"),
  MUSLIM("muslim.svg"),
  PLACE_OF_WORSHIP("place-of-worship.svg"),
  SHINTOIST("shintoist.svg"),
  SIKHIST("sikhist.svg"),
  TAOIST("taoist.svg"),

  // shop 
  ALCOHOL("alcohol.svg"),
  BAKERY("bakery.svg"),
  BEAUTY("beauty.svg"),
  BEVERAGES("beverages.svg"),
  BICYCLE("bicycle.svg"),
  BOOKS("books.svg"),
  BUTCHER("butcher.svg"),
  CAR("car.svg"),
  CAR_PARTS("car-parts.svg"),
  CHEMIST("chemist.svg"),
  CLOTHES("clothes.svg"),
  COMPUTER("computer.svg"),
  CONFECTIONERY("confectionery.svg"),
  CONVENIENCE("convenience.svg"),
  COPYSHOP("copyshop.svg"),
  DO_IT_YOURSELF("doityourself.svg"),
  ELECTRONICS("electronics.svg"),
  FLORIST("florist.svg"),
  FURNITURE("furniture.svg"),
  GARDEN_CENTER("garden-centre.svg"),
  GIFT("gift.svg"),
  GREEN_GROCER("greengrocer.svg"),
  HAIRDRESSER("hairdresser.svg"),
  HIFI("hifi.svg"),
  ICE_CREAM("ice-cream.svg"),
  JEWELLERY("jewellery.svg"),
  KIOSK("kiosk.svg"),
  LAUNDRY("laundry.svg"),
  MOBILE_PHONE("mobile-phone.svg"),
  NEWS("news.svg"),
  OPTICIAN("optician.svg"),
  PET("pet.svg"),
  PHOTO("photo.svg"),
  REPAIR_BICYCLE("repair-bicycle.svg"),
  REPAIR_CAR("repair-car.svg"),
  SHOES("shoes.svg"),
  STATIONARY("stationery.svg"),
  SUPERMARKET("supermarket.svg"),
  TOYS("toys.svg"),

  // sports
  SWIMMING("swimming.svg"),

  // tourism
  ARCHAEOLOGICAL_SITE("archaeological-site.svg"),
  CASTLE_DEFENSIVE("castle-defensive.svg"),
  CASTLE_FORTRESS("castle-fortress.svg"),
  CASTLE_MANOR("castle-manor.svg"),
  CASTLE_PALACE("castle-palace.svg"),
  CASTLE_STATELY("castle-stately.svg"),
  CITY_GATE("city-gate.svg"),
  FORT("fort.svg"),
  INFORMATION("information.svg"),
  MEMORIAL("memorial.svg"),
  MONUMENT("monument.svg"),
  MUSEUM("museum.svg"),
  THEATRE("theatre.svg"),
  VIEWPOINT("viewpoint.svg"),
  WATCHTOWER("watchtower.svg"),
  WINDMILL("windmill.svg"),

  // transport
  AIRPORT("airport.svg"),
  BUS_STATION("bus-station.svg"),
  BUS_STOP("bus-stop.svg"),
  CHARGING_STATION("charging-station.svg"),
  ELEVATOR("elevator.svg"),
  FERRY("ferry.svg"),
  FORD("ford.svg"),
  FUEL("fuel.svg"),
  HELIPORT("heliport.svg"),
  LUGGAGE("luggage.svg"),
  METRO("metro.svg"),
  PARKING_BICYCLE("parking-bicycle.svg"),
  PARKING_CAR("parking-car.svg"),
  PARKING_GARAGE_CAR("parking-garage-car.svg"),
  RAILWAY_HALT("railway-halt.svg"),
  RAILWAY_STATION("railway-station.svg"),
  RENTAL_BICYCLE("rental-bicycle.svg"),
  RENTAL_CAR("rental-car.svg"),
  SHARED_CAR("shared-car.svg"),
  TAXI("taxi.svg"),
  TRAM_STOP("tram-stop.svg");

  private static final String FOLDER = "/uploads/pictogram/";

  private final String _url;

  Pictogram(String url) {
    _url = url;
  }

  public String relativeUrl() {
    return FOLDER + _url;
  }
}
