package de.jmizv.umap.model;

import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
public abstract class MapProperties {

    @Value.Default
    public boolean easing() {
        return true;
    }

    @Value.Default
    public int smoothFactor() {
        return 3;
    }

    @Value.Default
    public boolean embedControl() {
        return true;
    }

    @Value.Default
    public boolean fullscreenControl() {
        return false;
    }

    @Value.Default
    public boolean searchControl() {
        return true;
    }

    @Value.Default
    public boolean datalayersControl() {
        return true;
    }

    @Value.Default
    public boolean zoomControl() {
        return true;
    }

    @Value.Default
    public SlideshowProperties slideshow() {
        return SlideshowProperties.ofDeactivated();
    }

    @Value.Default
    public boolean captionBar() {
        return false;
    }

    public Optional<LimitBoundsProperties> limitBounds() {
        return Optional.empty();
    }

    @Value.Default
    public TileLayer tilelayer() {
        return TileLayer.osmPositronCarto();
    }

    @Value.Default

    public String licence() {
        return "";
    }

    @Value.Default
    public String description() {
        return "";
    }

    public abstract String name();

    @Value.Default
    public boolean displayPopupFooter() {
        return false;
    }

    @Value.Default
    public boolean miniMap() {
        return false;
    }

    @Value.Default
    public boolean moreControl() {
        return true;
    }

    @Value.Default
    public boolean scaleControl() {
        return true;
    }

    @Value.Default
    public boolean scrollWheelZoom() {
        return true;
    }

    @Value.Default
    public int zoom() {
        return 12;
    }

    public static MapProperties ofName(String name) {
        return ImmutableMapProperties.builder().name(name).build();
    }
}
