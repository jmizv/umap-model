package de.jmizv.umap.model;

import org.immutables.value.Value;

@Value.Immutable
public abstract class LimitBoundsProperties {

    public abstract double south();

    public abstract double west();

    public abstract double north();

    public abstract double east();

    public static LimitBoundsProperties ofSWNE(double south, double west, double north, double east) {
        return ImmutableLimitBoundsProperties.builder()
                .south(south)
                .west(west)
                .north(north)
                .east(east)
                .build();
    }
}
