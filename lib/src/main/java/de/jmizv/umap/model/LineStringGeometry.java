package de.jmizv.umap.model;

import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
public abstract class LineStringGeometry extends Geometry {
    private final String type = "LineString";
    public abstract List<double[]> coordinates();

    public static LineStringGeometry of(List<double[]> coordinates) {
        return ImmutableLineStringGeometry.builder()
                .coordinates(coordinates)
                .build();
    }
}
