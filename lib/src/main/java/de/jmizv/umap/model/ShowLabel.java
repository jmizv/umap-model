package de.jmizv.umap.model;

public enum ShowLabel {

  ALWAYS("true"), NEVER("false"), HIDDEN("null");

  private final String _value;

  ShowLabel(String value) {
    _value = value;
  }

  @Override
  public String toString() {
    return _value;
  }
}
