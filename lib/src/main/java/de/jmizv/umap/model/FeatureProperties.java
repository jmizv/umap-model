package de.jmizv.umap.model;

import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
public abstract class FeatureProperties {
    public abstract String name();

    public abstract Optional<String> description();

    public abstract UmapOptions _umap_options();

    public static FeatureProperties of(String name, UmapOptions options) {
        return ImmutableFeatureProperties.builder().name(name)._umap_options(options).build();
    }

    public static FeatureProperties of(String name, String description, UmapOptions options) {
        return ImmutableFeatureProperties.builder().name(name).description(description)._umap_options(options).build();
    }
}
