package de.jmizv.umap.model;

import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
public abstract class PolygonGeometry extends Geometry {
    private final String type = "Polygon";
    public abstract List<List<double[]>> coordinates();

    public static PolygonGeometry of(List<List<double[]>> coordinates) {
        return ImmutablePolygonGeometry.builder()
                .coordinates(coordinates)
                .build();
    }
}
