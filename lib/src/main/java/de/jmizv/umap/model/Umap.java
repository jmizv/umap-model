package de.jmizv.umap.model;

import org.immutables.value.Value;

import java.util.List;
import java.util.Optional;

@Value.Immutable
public abstract class Umap {

    private final String type = "umap";

    public abstract Optional<String> uri();

    /**
     * The properties of the map. Contains for example the name and description of the map.
     */
    public abstract MapProperties properties();

    /**
     * The initial shown position on the map.
     */
    public abstract PointGeometry geometry();

    public abstract List<Layer> layers();

    public static Umap of(MapProperties properties, PointGeometry initialMapPosition, List<Layer> layers) {
        return ImmutableUmap.builder()
                .properties(properties)
                .geometry(initialMapPosition)
                .layers(layers)
                .build();
    }

    public static Umap of(String mapName, PointGeometry initialMapPosition, List<Layer> layers) {
        return of(MapProperties.ofName(mapName), initialMapPosition, layers);
    }
}

