package de.jmizv.umap.model;

import org.immutables.value.Value;

import java.util.Optional;


public abstract class UmapOptions {
  //public String color = "Black";
  //public float opacity = 0.2f;
  //public int weight = 1;
  //public boolean stroke = true;
  //public String fillColor = "Red";
  //public float fillOpacity = 0.5f;

  @Value.Immutable
  public abstract static class PolygonGeometryUmapOptions extends UmapOptions {
    public abstract String color();

    public static PolygonGeometryUmapOptions of(String color) {
      return ImmutablePolygonGeometryUmapOptions.builder().color(color).build();
    }

    public static PolygonGeometryUmapOptions ofBlack() {
      return of("Black");
    }
  }

  @Value.Immutable
  public abstract static class PointGeometryUmapOptions extends UmapOptions {
    public abstract Optional<String> iconUrl();

    public abstract Optional<IconClass> iconClass();

    /**
     * Must be a valid CSS value (eg.: DarkBlue or #123456)
     * @return the color of the point
     */
    @Value.Default
    public String color() {
      return "Black";
    }

    @Value.Default
    public float fillOpacity() {
      return 0.5f;
    }

    @Value.Default
    public float opacity() {
      return 0.7f;
    }

    @Value.Default
    public int weight() {
      return 4;
    }

    @Value.Default
    public ShowLabel showLabel() {
      return ShowLabel.NEVER;
    }

    public static PointGeometryUmapOptions ofIconUrl(String iconUrl) {
      return builder().iconUrl(iconUrl).build();
    }

    public static PointGeometryUmapOptions ofPictogram(Pictogram pictogram) {
      return builder().iconUrl(pictogram.relativeUrl()).build();
    }

    public static ImmutablePointGeometryUmapOptions.Builder builder() {
      return ImmutablePointGeometryUmapOptions.builder();
    }
  }

  @Value.Immutable
  public abstract static class FeatureCollectionUmapOptions extends UmapOptions {
    public abstract String name();

    @Value.Default
    public boolean displayOnLoad() {
      return true;
    }

    @Value.Default
    public boolean browsable() {
      return true;
    }

    public static FeatureCollectionUmapOptions of(String name) {
      return ImmutableFeatureCollectionUmapOptions.builder().name(name).build();
    }
  }

}
