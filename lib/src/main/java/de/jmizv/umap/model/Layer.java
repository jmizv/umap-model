package de.jmizv.umap.model;

import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
public abstract class Layer {
    private final String type = "FeatureCollection";
    public abstract List<Feature> features();

    public abstract UmapOptions _umap_options();

    public static Layer of(String name, List<Feature> features) {
        return ImmutableLayer.builder()
                .features(features)
                ._umap_options(UmapOptions.FeatureCollectionUmapOptions.of(name))
                .build();
    }

}
