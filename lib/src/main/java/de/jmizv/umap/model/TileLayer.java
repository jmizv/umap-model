package de.jmizv.umap.model;

import org.immutables.value.Value;

@Value.Immutable
public abstract class TileLayer {

    private static final String OSM_POSITRON_NAME = "OSM Positron (Carto)";
    private static final String OSM_POSITRON_URL_TEMPLATE = "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png";
    private static final String OSM_POSITRON_ATTRIBUTION = "Map tiles by [[http://cartodb.com/attributions#basemaps|CartoDB]], under [[https://creativecommons.org/licenses/by/3.0/|CC BY 3.0]]. map data © [[http://osm.org/copyright|OpenStreetMap contributors]] under ODbL ";

    private static final String OSM_LIGHT_NAME = "OSM Light (jawgmaps)";
    private static final String OSM_LIGHT_URL_TEMPLATE = "https://tile.jawg.io/light/{z}/{x}/{y}.png?api-key=community";
    private static final String OSM_LIGHT_ATTRIBUTION = "Tiles courtesy of [[https://www.jawg.io/|jawgmaps]] - Map data [[http://osm.org/copyright/|&copy; OpenStreetMap contributors]], under ODbL.";

    private static final TileLayer OSM_POSITRON_CARTO = builder()
            .name(OSM_POSITRON_NAME)
            .url_template(OSM_POSITRON_URL_TEMPLATE)
            .attribution(OSM_POSITRON_ATTRIBUTION)
            .build();

    private static final TileLayer OSM_LIGHT = builder()
            .name(OSM_LIGHT_NAME)
            .url_template(OSM_LIGHT_URL_TEMPLATE)
            .attribution(OSM_LIGHT_ATTRIBUTION)
            .maxZoom(18)
            .build();

    @Value.Default
    public int minZoom() {
        return 0;
    }

    @Value.Default
    public int maxZoom() {
        return 20;
    }

    public abstract String attribution();

    public abstract String url_template();

    public abstract String name();

    @Value.Default
    public boolean tms() {
        return false;
    }

    public static TileLayer osmPositronCarto() {
        return OSM_POSITRON_CARTO;
    }

    public static TileLayer osmLight() {
        return OSM_LIGHT;
    }

    private static ImmutableTileLayer.Builder builder() {
        return ImmutableTileLayer.builder();
    }
}
